<?php
namespace TYPO3\BccVoting\Tests\Unit\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.BccVoting".            *
 *                                                                        *
 *                                                                        */

/**
 * Testcase for Budget
 */
class BudgetTest extends \TYPO3\FLOW3\Tests\UnitTestCase {

	/**
	 * @test
	 */
	public function makeSureThatSomethingHolds() {
		$this->markTestIncomplete('Automatically generated test case; you need to adjust this!');

		$expected = 'Foo';
		$actual = 'Foo'; // This should be the result of some function call
		$this->assertSame($expected, $actual);
	}
}
?>