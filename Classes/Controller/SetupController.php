<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Sets up dummy data for testing
 */
class SetupController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\CircularRepository
	 */
	protected $circularRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectionRepository
	 */
	protected $electionRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorRepository
	 */
	protected $electorRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 */
	protected $electorateRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\NomineeRepository
	 */
	protected $nomineeRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Security\AccountRepository
	 */
	protected $accountRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Security\Authentication\AuthenticationManagerInterface
	 */
	protected $authenticationManager;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Security\Context
	 */
	protected $securityContext;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Mvc\Routing\ObjectPathMappingRepository
	 */
	protected $objectPathMappingRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Security\AccountFactory
	 */
	protected $accountFactory;

	/**
	 * @return void
	 */
	public function indexAction() {

	}

	/**
	 * @return void
	 */
	public function createDataAction() {
		if ($this->electionRepository->countAll() > 0) {
			$this->addFlashMessage('Please empty the database before creating new data!');
			$this->redirect('index');
		}
		$electorate1 = $this->createElectorate();
		for($i = 0; $i < 10; $i ++) {
			$elector = $this->createElector();
			$this->electorRepository->add($elector);
			$electorate1->addElector($elector);
		}
		$this->electorateRepository->add($electorate1);

		$election1 = $this->createElection($electorate1);
		for($i = 0; $i < 4; $i ++) {
			$nominee = $this->createNominee();
			$this->nomineeRepository->add($nominee);
			if ($nominee->isPublic() === TRUE) {
				$election1->addNominee($nominee);
			}
		}
		$this->electionRepository->add($election1);

		$this->addFlashMessage('Dummy data created');
		$this->redirect('index');
	}

	/**
	 * @return void
	 */
	public function showRoutingCacheAction() {
		// TODO fix me
		$this->view->assign('objectPathMappings', $this->objectPathMappingRepository->findAll());
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Electorate
	 */
	protected function createElectorate() {
		$electorate = new \TYPO3\BccVoting\Domain\Model\Electorate();
		$electorate->setTitle('Test List');
		return $electorate;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Elector
	 */
	protected function createElector() {
		$elector = new \TYPO3\BccVoting\Domain\Model\Elector();
		$elector->setFirstName(\TYPO3\Faker\Name::firstName());
		if (mt_rand(0, 1)) {
			$elector->setMiddleName(\TYPO3\Faker\Name::firstName());
		}
		$elector->setLastName(\TYPO3\Faker\Name::lastName());
		$elector->setEmail(\TYPO3\Faker\Internet::email($elector->getFullName()));
		$elector->setGender(mt_rand(0, 1) ? 'male' : 'female');
		return $elector;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 * @return \TYPO3\BccVoting\Domain\Model\Election
	 */
	protected function createElection(\TYPO3\BccVoting\Domain\Model\Electorate $electorate) {
		$election = new \TYPO3\BccVoting\Domain\Model\Election();
		$election->setTitle('Test Election');
		$election->setStartDate(\TYPO3\Faker\Date::random('0', '+ 1 week'));
		$election->setEndDate(\TYPO3\Faker\Date::random('+ 1 week', '+ 3 week'));
		$election->setDescription(\TYPO3\Faker\Lorem::sentence(3));
		$election->setElectorate($electorate);
		return $election;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Nominee
	 */
	protected function createNominee() {
		$nominee = new \TYPO3\BccVoting\Domain\Model\Nominee();
		$nominee->setFirstName(\TYPO3\Faker\Name::firstName());
		if (mt_rand(0, 1)) {
			$nominee->setMiddleName(\TYPO3\Faker\Name::firstName());
		}
		$nominee->setLastName(\TYPO3\Faker\Name::lastName());
		$nominee->setEmail(\TYPO3\Faker\Internet::email($nominee->getFullName()));
		$nominee->setUrl('http://' . \TYPO3\Faker\Internet::domainName());
		$nominee->setGender(mt_rand(0, 1) ? 'male' : 'female');
		$nominee->setPublic(mt_rand(0, 1) === 1);
		return $nominee;
	}

	/**
	 * @param string $username
	 * @FLOW3\IgnoreValidation("$username")
	 * @return void
	 */
	public function newSupervisorAction($username = NULL) {
		if ($this->accountRepository->countAll() > 0) {
			$this->addFlashMessage('Supervisor account has been created already!');
			$this->redirect('index');
		}
		$this->view->assign('username', $username);
	}

	/**
	 * @param string $username
	 * @param string $password
	 	 * @FLOW3\Validate("$username", type="Alphanumeric")
	 * @FLOW3\Validate("$username", type="StringLength", options={ "minimum"=3, "maximum"=15 })
	 	 * @FLOW3\Validate("$password", type="StringLength", options={ "minimum"=6, "maximum"=30 })
	 * @return void
	 */
	public function createSupervisorAction($username, $password) {
		if ($this->accountRepository->countAll() > 0) {
			$this->addFlashMessage('Supervisor account has been created already!');
			$this->redirect('index');
		}
		$account = $this->accountFactory->createAccountWithPassword($username, $password, array('Supervisor'));
		$this->accountRepository->add($account);

		$authenticationTokens = $this->securityContext->getAuthenticationTokensOfType('TYPO3\FLOW3\Security\Authentication\Token\UsernamePassword');
		if (count($authenticationTokens) === 1) {
			$authenticationTokens[0]->setAccount($account);
			$authenticationTokens[0]->setAuthenticationStatus(\TYPO3\FLOW3\Security\Authentication\TokenInterface::AUTHENTICATION_SUCCESSFUL);
		}
		$this->addFlashMessage('Supervisor account %s has been created', 'Account created', \TYPO3\FLOW3\Error\Message::SEVERITY_OK, array(htmlspecialchars($username)));

		$this->redirect('index', 'Standard');
	}

}

?>