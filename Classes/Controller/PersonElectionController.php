<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Election Controller
 */
class PersonElectionController extends ElectionController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\PersonElectionRepository
	 */
	protected $personElectionRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\PersonVoteRepository
	 */
	protected $personVoteRepository;

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @param string $code
	 * @return void
	 */
	public function showAction(\TYPO3\BccVoting\Domain\Model\Election $election, $code = NULL) {
		$this->view->assign('election', $election);
		$this->view->assign('code', $code);
	}


	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return void
	 */
	public function showResultsAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		if ($election->isActive()) {
			$this->addFlashMessage('Election is still active');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election));
		}
		$votingResult = $this->personVoteRepository->getVotingResult($election);
		$this->view->assign('election', $election);
		$this->view->assign('votingResult', $votingResult);
	}


		/**
	 * @param \TYPO3\BccVoting\Domain\Model\PersonElection $newPersonElection
	 * @return void
	 * @FLOW3\IgnoreValidation("$newPersonElection")
	 */
	public function newAction(\TYPO3\BccVoting\Domain\Model\PersonElection $newPersonElection = NULL) {
		$this->view->assign('numberOfVotesOptions', $this->getNumberOfVotesOptions($newPersonElection));
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('publicNominees', $this->nomineeRepository->findByPublic(TRUE));
		$this->view->assign('newPersonElection', $newPersonElection);
	}


	/**
	 * @param \TYPO3\BccVoting\Domain\Model\PersonElection $newPersonElection
	 * @return void
	 */
	public function createAction(\TYPO3\BccVoting\Domain\Model\PersonElection $newPersonElection) {
		$this->electionRepository->add($newPersonElection);
		$this->addFlashMessage('Election has been created');
		$this->redirect('index','Election');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @FLOW3\IgnoreValidation("$election")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		$this->view->assign('numberOfVotesOptions', $this->getNumberOfVotesOptions($election));
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('publicNominees', $this->nomineeRepository->findByPublic(TRUE));
		$this->view->assign('election', $election);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\PersonElection $exlection
	 * @return void
	 */
	public function updateAction(\TYPO3\BccVoting\Domain\Model\PersonElection $election) {
		$this->personElectionRepository->update($election);
		$this->addFlashMessage('Election has been updated');
		$this->redirect('index', 'Election');
	}

	/**
	 * @param Election $election
	 * @return void
	 */
	protected function getNumberOfVotesOptions(\TYPO3\BccVoting\Domain\Model\Election $election = NULL) {
		$numberOfVotesOptions = array();
		$numberOfNominees = $election !== NULL ? count($election->getNominees()) : 0;
		if ($numberOfNominees > 1) {
			for($numberOfVotes = 1; $numberOfVotes <= $numberOfNominees; $numberOfVotes++) {
				$numberOfVotesOptions[$numberOfVotes] = $numberOfVotes;
			}
		} else {
			$numberOfVotesOptions[1] = 1;
		}
		$numberOfVotesOptions['<# of Nominees>'] = 0;
		return $numberOfVotesOptions;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\PersonVote $personVote
	 * @param string $code
	 * @return void
	 * @FLOW3\IgnoreValidation("$personVote")
	 */
	public function voteAction(\TYPO3\BccVoting\Domain\Model\PersonVote $personVote, $code) {
		// TODO this should be handled by the security framework!
		$election = $personVote->getElection();
		try {
			$token = $this->validateToken($code, $election);
		} catch (\TYPO3\BccVoting\Exception\InvalidTokenException $exception) {
			$this->addFlashMessage('Invalid token (code ' . $exception->getCode() . ')');
			throw $exception;
		}
		if (!$election->isActive()) {
			$this->addFlashMessage('This election is not active any more!');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election, 'code' => $code));
		}
		$numberOfVotes = count($personVote->getNominees());
		if ($numberOfVotes < 1) {
			$this->addFlashMessage('You have to vote for one nominee at least!');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election, 'code' => $code));
		}
		$maxNumberOfVotes = $election->getMaxNumberOfVotes();
		if ($maxNumberOfVotes > 0 && $numberOfVotes > $maxNumberOfVotes) {
			$this->addFlashMessage('You can only vote for up to ' . $maxNumberOfVotes . ' nominees in this election!');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election, 'code' => $code));
		}
		$allowedNominees = $election->getNominees();
		foreach($personVote->getNominees() as $nominee) {
			if (!$allowedNominees->contains($nominee)) {
				throw new \TYPO3\FLOW3\Exception('Something went wrong..', 1287155383);
			}
		}
		$this->personVoteRepository->add($personVote);
		$this->tokenRepository->remove($token);
		$this->addFlashMessage('Thank you for the voting!');
		$this->redirect('confirmation', 'Election', 'TYPO3.BccVoting', array('election' => $election));
	}
}
?>