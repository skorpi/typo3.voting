<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Election Controller
 */
class ElectionController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectionRepository
	 */
	protected $electionRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 */
	protected $electorateRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\NomineeRepository
	 */
	protected $nomineeRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\TokenRepository
	 */
	protected $tokenRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\VoteRepository
	 */
	protected $voteRepository;

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('activeElections', $this->electionRepository->findActive());
		$this->view->assign('upcomingElections', $this->electionRepository->findUpcoming());
		$this->view->assign('expiredElections', $this->electionRepository->findExpired());
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @param string $code
	 * @return void
	 */
	public function showAction(\TYPO3\BccVoting\Domain\Model\Election $election, $code = NULL) {

		// TODO this should be handled by the security framework!
		if ($code !== NULL) {
			try {
				$this->validateToken($code, $election);
				//$this->view->assign('code', $code);
			} catch (\TYPO3\BccVoting\Exception\InvalidTokenException $exception) {
				$this->addFlashMessage('Invalid token (code ' . $exception->getCode() . ')');
				//throw $exception;
			}
		}
		$this->forward('show', $this->getElectionControllerName($election), 'TYPO3.BccVoting', array('election' => $election, 'code' => $code));
	}


	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return void
	 */
	public function confirmationAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		$this->view->assign('election', $election);
	}

	/**
	 * @throws \TYPO3\BccVoting\Exception\InvalidTokenException
	 * @param string $code
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return \TYPO3\BccVoting\Domain\Model\Token
	 */
	protected function validateToken($code, \TYPO3\BccVoting\Domain\Model\Election $election) {
		if (strlen($code) < 5) {
			throw new \TYPO3\BccVoting\Exception\InvalidTokenException('invalid code', 1287152941);
		}
		$token = $this->tokenRepository->findOneByCode($code);
		if (!$token instanceof \TYPO3\BccVoting\Domain\Model\Token) {
			throw new \TYPO3\BccVoting\Exception\InvalidTokenException('non existing code', 1287152942);
		}
		if ($token->getElection() !== $election) {
			throw new \TYPO3\BccVoting\Exception\InvalidTokenException('token not valid for this election', 1287152943);
		}
		return $token;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return void
	 */
	public function showResultsAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		if ($election->isActive()) {
			$this->addFlashMessage('Election is still active');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election));
		}
		$this->forward('showResults', $this->getElectionControllerName($election), 'TYPO3.BccVoting', array('election' => $election));
	}


	/**
	 * @return void
	 */
	public function initializeCreateAction() {
		// TODO why cant this be set globally?
//		$this->arguments['newElection']
//	 		->getPropertyMappingConfiguration()
//	 		->forProperty('startDate')
//	 		->setTypeConverterOption(
//				'TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter',
//				\TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
//				'U'
//		);
//		$this->arguments['newElection']
//	 		->getPropertyMappingConfiguration()
//	 		->forProperty('endDate')
//	 		->setTypeConverterOption(
//				'TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter',
//				\TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
//				'U'
//		);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @FLOW3\IgnoreValidation("$election")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Election $election) {

		$this->forward('edit', $this->getElectionControllerName($election), 'TYPO3.BccVoting', array('election' => $election));
/*		$this->view->assign('numberOfVotesOptions', $this->getNumberOfVotesOptions($election));
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('publicNominees', $this->nomineeRepository->findByPublic(TRUE));
		$this->view->assign('election', $election);*/
	}

	/**
	 * @return void
	 */
	public function initializeUpdateAction() {
//		// TODO why cant this be set globally?
//		$this->arguments['election']
//	 		->getPropertyMappingConfiguration()
//	 		->forProperty('startDate')
//	 		->setTypeConverterOption(
//				'TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter',
//				\TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
//				'U'
//		);
//		$this->arguments['election']
//	 		->getPropertyMappingConfiguration()
//	 		->forProperty('endDate')
//	 		->setTypeConverterOption(
//				'TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter',
//				\TYPO3\FLOW3\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT,
//				'U'
//		);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return void
	 */
	public function deleteAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		$this->electionRepository->remove($election);
		$this->addFlashMessage('Election has been deleted');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return string
	 */
	protected function getElectionControllerName(\TYPO3\BccVoting\Domain\Model\Election $election) {
		return $election->getType() . 'Election';
	}

}
?>