<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.BccVoting".            *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Budget controller for the TYPO3.BccVoting package
 *
 * @FLOW3\Scope("singleton")
 */
class BudgetController extends \TYPO3\FLOW3\Mvc\Controller\ActionController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\BudgetRepository
	 */
	protected $budgetRepository;

	/**
	 * Index action
	 *
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('budgets', $this->budgetRepository->findAll());
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $newBudget
	 * @return void
	 * @FLOW3\IgnoreValidation("$newBudget")
	 */
	public function newAction(\TYPO3\BccVoting\Domain\Model\Budget $newBudget = NULL) {
		$this->view->assign('newBudget', $newBudget);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $newBudget
	 * @return void
	 */
	public function createAction(\TYPO3\BccVoting\Domain\Model\Budget $newBudget) {
		$this->budgetRepository->add($newBudget);
		$this->addFlashMessage('Budget has been created');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $budget
	 * @FLOW3\IgnoreValidation("$budget")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Budget $budget) {
		$this->view->assign('budget', $budget);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $budget
	 * @return void
	 */
	public function updateAction(\TYPO3\BccVoting\Domain\Model\Budget $budget) {
		$this->budgetRepository->update($budget);
		$this->addFlashMessage('Budget has been updated');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $budget
	 * @return void
	 */
	public function deleteAction(\TYPO3\BccVoting\Domain\Model\Budget $budget) {
		$this->budgetRepository->remove($budget);
		$this->addFlashMessage('Budget has been deleted');
		$this->redirect('index');
	}

}

?>