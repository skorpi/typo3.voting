<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Election Controller
 */
class BudgetElectionController extends ElectionController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\BudgetElectionRepository
	 */
	protected $budgetElectionRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\BudgetRepository
	 */
	protected $budgetRepository;


	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\BudgetVoteRepository
	 */
	protected $budgetVoteRepository;


	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\TokenRepository
	 */
	protected $tokenRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Service\BudgetVotingResultService
	 */
	protected $budgetVotingResultService;

	/**
	 * @var \TYPO3\BccVoting\Service\CsvService
	 * @FLOW3\Inject
	 */
	protected $csvService;

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @param string $code
	 * @return void
	 */
	public function showAction(\TYPO3\BccVoting\Domain\Model\Election $election, $code = NULL) {
		$this->view->assign('election', $election);
		$this->view->assign('code', $code);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return void
	 */
	public function showResultsAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		if ($election->isActive()) {
			$this->addFlashMessage('Election is still active');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election));
		}
		$votingResult = $this->budgetVotingResultService->generateVotingResult($election);

		$this->view->assign('election', $election);
		$this->view->assign('votingResult', $votingResult);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @return void
	 */
	public function showCsvAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		if ($election->isActive()) {
			$this->addFlashMessage('Election is still active');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election));
		}
		$votingResult = $this->budgetVotingResultService->generateVotingResult($election);
		$resultArray = $votingResult->getBudgetResults();
		$resultCsv = $this->csvService->convertFromArray($resultArray,array(
			'budget', 'amountNegativeVotes', 'amountNeutralVotes', 'amountPositiveVotes', 'sum'
		));
		$this->view->assign('csv', $resultCsv);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\BudgetElection $newBudgetElection
	 * @return void
	 * @FLOW3\IgnoreValidation("$newBudgetElection")
	 */
	public function newAction(\TYPO3\BccVoting\Domain\Model\BudgetElection $newBudgetElection = NULL) {
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('budgets', $this->budgetRepository->findAll());
		$this->view->assign('newBudgetElection', $newBudgetElection);
	}


	/**
	 * @param \TYPO3\BccVoting\Domain\Model\BudgetElection $newBudgetElection
	 * @return void
	 */
	public function createAction(\TYPO3\BccVoting\Domain\Model\BudgetElection $newBudgetElection) {
		$this->electionRepository->add($newBudgetElection);
		$this->addFlashMessage('Election has been created');
		$this->redirect('index','Election');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 * @FLOW3\IgnoreValidation("$election")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Election $election) {
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('budgets', $this->budgetRepository->findAll());
		$this->view->assign('election', $election);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\BudgetElection $election
	 * @return void
	 */
	public function updateAction(\TYPO3\BccVoting\Domain\Model\BudgetElection $election) {
		$this->budgetElectionRepository->update($election);
		$this->addFlashMessage('Election has been updated');
		$this->redirect('index', 'Election');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\BudgetVote $budgetVote
	 * @param string $code
	 * @return void
	 */
	public function voteAction(\TYPO3\BccVoting\Domain\Model\BudgetVote $budgetVote, $code) {
		// TODO this should be handled by the security framework!
		$election = $budgetVote->getElection();
		try {
			$token = $this->validateToken($code, $election);
		} catch (\TYPO3\BccVoting\Exception\InvalidTokenException $exception) {
			$this->addFlashMessage('Invalid token (code ' . $exception->getCode() . ')');
			throw $exception;
		}
		if (!$election->isActive()) {
			$this->addFlashMessage('This election is not active any more!');
			$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $election, 'code' => $code));
		}

		foreach ($budgetVote->getBudgetRatings() as $budgetRating) {
			$budgetRating->setBudgetVote($budgetVote);
		}

		$this->budgetVoteRepository->add($budgetVote);
		$this->tokenRepository->remove($token);
		$this->addFlashMessage('Thank you for the voting!');
		$this->redirect('confirmation', 'Election', 'TYPO3.BccVoting', array('election' => $election));
	}

}
?>