<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Nominee Controller
 */
class NomineeController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\NomineeRepository
	 */
	protected $nomineeRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\Fluid\Core\Parser\TemplateParser
	 */
	protected $templateParser;

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('publicNominees', $this->nomineeRepository->findByPublic(TRUE));
		$this->view->assign('hiddenNominees', $this->nomineeRepository->findByPublic(FALSE));
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $newNominee
	 * @return void
	 * @FLOW3\IgnoreValidation("$newNominee")
	 */
	public function newAction(\TYPO3\BccVoting\Domain\Model\Nominee $newNominee = NULL) {
		$this->view->assign('newNominee', $newNominee);
	}

	/**
	 * @return void
	 */
	public function initializeCreateAction() {
		$this->arguments['newNominee']->getPropertyMappingConfiguration()->allowCreationForSubProperty('image');
		$this->arguments['newNominee']->getPropertyMappingConfiguration()->allowProperties('gender');
		$this->arguments['newNominee']->getPropertyMappingConfiguration()->allowProperties('public');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $newNominee
	 * @return void
	 */
	public function createAction(\TYPO3\BccVoting\Domain\Model\Nominee $newNominee) {
		$this->nomineeRepository->add($newNominee);
		$this->addFlashMessage('Nominee has been created');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $nominee
	 * @FLOW3\IgnoreValidation("$nominee")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Nominee $nominee) {
		$this->view->assign('nominee', $nominee);
	}

	/**
	 * @return void
	 */
	public function initializeUpdateAction() {
		$this->arguments['nominee']->getPropertyMappingConfiguration()->allowCreationForSubProperty('image');
		$this->arguments['nominee']->getPropertyMappingConfiguration()->allowModificationForSubProperty('image');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $nominee
	 * @return void
	 */
	public function updateAction(\TYPO3\BccVoting\Domain\Model\Nominee $nominee) {
		$this->nomineeRepository->update($nominee);
		$this->addFlashMessage('Nominee has been updated');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $nominee
	 * @return void
	 */
	public function deleteAction(\TYPO3\BccVoting\Domain\Model\Nominee $nominee) {
		$this->nomineeRepository->remove($nominee);
		$this->addFlashMessage('Nominee has been deleted');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $newNominee
	 * @return void
	 * @FLOW3\IgnoreValidation("$newNominee")
	 */
	public function applyAction(\TYPO3\BccVoting\Domain\Model\Nominee $newNominee = NULL) {
		$this->view->assign('newNominee', $newNominee);
	}

	/**
	 * @return void
	 */
	public function initializeCreateApplicationAction() {
		$this->arguments['newNominee']->getPropertyMappingConfiguration()->allowCreationForSubProperty('image');
		$this->arguments['newNominee']->getPropertyMappingConfiguration()->allowProperties('gender');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $newNominee
	 * @return void
	 */
	public function createApplicationAction(\TYPO3\BccVoting\Domain\Model\Nominee $newNominee) {
		$existingNominee = $this->nomineeRepository->findOneByEmail($newNominee->getEmail());
		if ($existingNominee !== NULL) {
			$this->addFlashMessage('This email is already registered!');
			$this->forward('apply', NULL, NULL, array('newNominee' => $newNominee));
		}
		$newNominee->setPublic(FALSE);

		// Notification email:
		$notificationBody = $this->renderFluidTemplate('resource://TYPO3.BccVoting/Private/Templates/Nominee/Email_ApplicationNotification.html', array('nominee' => $newNominee));
		$this->sendEmail(array($this->settings['email']['adminRecipientEmail'] => $this->settings['email']['adminName']), $this->settings['email']['nominee']['ApplicationNotificationSubject'], $notificationBody, array($newNominee->getEmail() => $newNominee->getFullName()));

		// Confirmation email:
		$confirmationBody = $this->renderFluidTemplate('resource://TYPO3.BccVoting/Private/Templates/Nominee/Email_ApplicationConfirmation.html', array('nominee' => $newNominee));
		$this->sendEmail(array($newNominee->getEmail() => $newNominee->getFullName()), $this->settings['email']['nominee']['ApplicationConfirmationSubject'], $confirmationBody, array($this->settings['email']['adminRecipientEmail'] => $this->settings['email']['adminName']));

		$this->nomineeRepository->add($newNominee);
		$this->addFlashMessage('Thank you for your application!');
		$this->redirect('index', 'Standard');
	}

}
?>