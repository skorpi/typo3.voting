<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A controller which allows for logging into the system
 */
class LoginController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Security\Authentication\AuthenticationManagerInterface
	 */
	protected $authenticationManager;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Security\Context
	 */
	protected $securityContext;

	/**
	 *
	 *
	 * @return string
	 */
	public function indexAction() {

	}

	/**
	 * Authenticates an account by invoking the Provider based Authentication Manager.
	 *
	 * @return void
	 */
	public function authenticateAction() {
		try {
			$this->authenticationManager->authenticate();
			$this->redirect('index', 'Standard');
		} catch (\TYPO3\FLOW3\Security\Exception\AuthenticationRequiredException $exception) {
			$this->addFlashMessage('Wrong username or password.');
			throw $exception;
		}
	}

	/**
	 * Authenticates an account by given token code
	 *
	 * @param string $code
	 * @return void
	 */
	public function tokenLoginAction($code) {
		// TODO FIX
		die('NOT IMPLEMENTED');

		$account = new \TYPO3\FLOW3\Security\Account();
		$credentials = md5(md5('elector') . 'someSalt') . ',someSalt';

		$roles = array(
			new \TYPO3\FLOW3\Security\Policy\Role('Elector'),
		);

		$this->authenticationManager->logout();

		$account->setAccountIdentifier('elector');
		$account->setCredentialsSource($credentials);
		$account->setAuthenticationProviderName('DefaultProvider');
		$account->setRoles($roles);
		$authenticationTokens = $this->securityContext->getAuthenticationTokensOfType('TYPO3\FLOW3\Security\Authentication\Token\UsernamePassword');
		if (count($authenticationTokens) === 1) {
			$authenticationTokens[0]->setAccount($account);
			$authenticationTokens[0]->setAuthenticationStatus(\TYPO3\FLOW3\Security\Authentication\TokenInterface::AUTHENTICATION_SUCCESSFUL);
		}
		$this->redirect('show', 'Election', 'TYPO3.BccVoting', array('election' => $token->getElection()));
	}

	/**
	 *
	 * @return void
	 */
	public function logoutAction() {
		$this->authenticationManager->logout();
		$this->addFlashMessage('Successfully logged out.');
		$this->redirect('index', 'Standard');
	}
}

?>