<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Electorate Controller
 */
class ElectorateController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 */
	protected $electorateRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorRepository
	 */
	protected $electorRepository;

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('electorates', $this->electorateRepository->findAll());
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $newElectorate
	 * @return void
	 * @FLOW3\IgnoreValidation("$newElectorate")
	 */
	public function newAction(\TYPO3\BccVoting\Domain\Model\Electorate $newElectorate = NULL) {
		$this->view->assign('newElectorate', $newElectorate);
		$this->view->assign('electors', $this->electorRepository->findAll());
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $newElectorate
	 * @return void
	 */
	public function createAction(\TYPO3\BccVoting\Domain\Model\Electorate $newElectorate) {
		$this->electorateRepository->add($newElectorate);
		$this->addFlashMessage('Electorate has been created');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 * @FLOW3\IgnoreValidation("$electorate")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Electorate $electorate) {
		$this->view->assign('electorate', $electorate);
		$this->view->assign('electors', $this->electorRepository->findAll());
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $currentElectorate
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @FLOW3\IgnoreValidation("$electorate")
	 * @return void
	 */
	public function addElectorAction(\TYPO3\BccVoting\Domain\Model\Electorate $currentElectorate, \TYPO3\BccVoting\Domain\Model\Elector $elector) {
		$currentElectorate->addElector($elector);
		$this->addFlashMessage('Elector has been added to electorate');
		$this->redirect('edit', NULL, NULL, array('electorate' => $currentElectorate));
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $currentElectorate
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @FLOW3\IgnoreValidation("$electorate")
	 * @return void
	 */
	public function removeElectorAction(\TYPO3\BccVoting\Domain\Model\Electorate $currentElectorate, \TYPO3\BccVoting\Domain\Model\Elector $elector) {
		$currentElectorate->removeElector($elector);
		$this->electorateRepository->update($currentElectorate);
		$this->addFlashMessage('Elector has been removed from electorate');
		$this->redirect('edit', NULL, NULL, array('electorate' => $currentElectorate));
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 * @return void
	 */
	public function updateAction(\TYPO3\BccVoting\Domain\Model\Electorate $electorate) {
		$this->electorateRepository->update($electorate);
		$this->addFlashMessage('Electorate has been updated');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 * @return void
	 */
	public function deleteAction(\TYPO3\BccVoting\Domain\Model\Electorate $electorate) {
		$this->electorateRepository->remove($electorate);
		$this->addFlashMessage('Electorate has been deleted');
		$this->redirect('index');
	}
}
?>