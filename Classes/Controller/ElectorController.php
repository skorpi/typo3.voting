<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Elector Controller
 */
class ElectorController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorRepository
	 */
	protected $electorRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 */
	protected $electorateRepository;

	/**
	 * @var \TYPO3\BccVoting\Service\CsvService
	 * @FLOW3\Inject
	 */
	protected $csvService;

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('electors', $this->electorRepository->findAll());
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $newElector
	 * @return void
	 * @FLOW3\IgnoreValidation("$newElector")
	 */
	public function newAction(\TYPO3\BccVoting\Domain\Model\Elector $newElector = NULL) {
		$this->view->assign('newElector', $newElector);
	}

	/**
	 * @return void
	 */
	public function initializeCreateAction() {
		$this->arguments['newElector']->getPropertyMappingConfiguration()->allowProperties('gender');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $newElector
	 * @return void
	 * TODO: refactor
	 */
	public function createAction(\TYPO3\BccVoting\Domain\Model\Elector $newElector) {
		$existingElector = $this->electorRepository->findOneByEmail($newElector->getEmail());
		if ($existingElector !== NULL) {
			$this->addFlashMessage('Elector already exists');
		} else {
			$this->electorRepository->add($newElector);
			$this->addFlashMessage('Elector has been created');
		}
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @FLOW3\IgnoreValidation("$elector")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Elector $elector) {
		$this->view->assign('elector', $elector);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @return void
	 */
	public function updateAction(\TYPO3\BccVoting\Domain\Model\Elector $elector) {
		// TODO: make sure, that you can's change the email to an existing email address
		$this->electorRepository->update($elector);
		$this->addFlashMessage('Elector has been updated');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @return void
	 */
	public function deleteAction(\TYPO3\BccVoting\Domain\Model\Elector $elector) {
		$this->electorRepository->remove($elector);
		$this->addFlashMessage('Elector has been deleted');
		$this->redirect('index');
	}

	/**
	 * @return void
	 */
	public function deleteAllAction() {
		$this->electorRepository->removeAll();
		$this->addFlashMessage('All electors have been deleted');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $currentElectorate
	 * @return void
	 */
	public function importformAction(\TYPO3\BccVoting\Domain\Model\Electorate $currentElectorate = NULL) {
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('currentElectorate', $currentElectorate);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\ElectorImport $electorImport
	 * @return void
	 */
	public function importAction(\TYPO3\BccVoting\Domain\Model\ElectorImport $electorImport) {
		$csv = file_get_contents('resource://' . $electorImport->getCsvResource()->getResourcePointer());
		$electorArray = $this->csvService->convertToArray($csv);
		$importCount = 0;
		$skipCount = 0;
		foreach($electorArray as $electorItem) {
			// TODO: remove this work around
			$elector = $this->electorRepository->findOneByEmail($electorItem['email']);
			if ($elector === NULL) {
				$elector = new \TYPO3\BccVoting\Domain\Model\Elector();
				$firstName = isset($electorItem['firstName']) ? $electorItem['firstName'] : '';
				$middleName = isset($electorItem['middleName']) ? $electorItem['middleName'] : '';
				$lastName = isset($electorItem['lastName']) ? $electorItem['lastName'] : '';
				$elector->setFirstName($firstName);
				$elector->setMiddleName($middleName);
				$elector->setLastName($lastName);
				$elector->setEmail($electorItem['email']);
				$gender = (isset($electorItem['gender']) && strtolower($electorItem['gender']) === 'female') ? \TYPO3\BccVoting\Domain\Model\Elector::GENDER_FEMALE : \TYPO3\BccVoting\Domain\Model\Elector::GENDER_MALE;
				$elector->setGender($gender);
				$this->electorRepository->add($elector);
				$importCount ++;
			} else {
				$skipCount ++;
			}
			if ($electorImport->getElectorate() !== NULL) {
				$electorImport->getElectorate()->addElector($elector);
			}
		}
		$this->addFlashMessage('Imported ' . $importCount . ' electors (skipped ' . $skipCount .')');
		if ($electorImport->getElectorate() !== NULL) {
			$this->electorateRepository->update($electorImport->getElectorate());
			$this->redirect('edit', 'Electorate', NULL, array('electorate' => $electorImport->getElectorate()));
		}
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 * @return void
	 */
	public function exportAction(\TYPO3\BccVoting\Domain\Model\Electorate $electorate = NULL) {
		$electors = array();
		if ($electorate !== NULL) {
			$electors = $electorate->getElectors();
			$fileName = sprintf('%s_%s_Electors.csv', date('Y-m-d'), str_replace(' ', '-', $electorate->getTitle()));
		} else {
			$electors = $this->electorRepository->findAll();
			$fileName = sprintf('%s_Electors.csv', date('Y-m-d'));
		}
		if (!is_array($electors)) {
			$electors = iterator_to_array($electors);
		}
		$csv = $this->csvService->convertFromArray($electors, array('firstName', 'middleName', 'lastName', 'gender', 'email'));
		header('Content-type: application/force-download');
		header('Content-Transfer-Encoding: Binary');
		header('Content-length: ' . strlen($csv));
		header('Content-disposition: attachment; filename="' . $fileName . '"');
		echo $csv;
		exit;
	}
}
?>