<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Base controller for all controllers of this package
 */
abstract class AbstractBaseController extends \TYPO3\FLOW3\Mvc\Controller\ActionController {

	/**
	 * @var \TYPO3\FLOW3\Log\SystemLoggerInterface
	 * @FLOW3\Inject
	 */
	protected $systemLogger;

	/**
	 * @return string|boolean The flash message or FALSE if no flash message should be set
	 */
	protected function getErrorFlashMessage() {
		return FALSE;
	}

	/**
	 * @param array $recipient
	 * @param string $subject
	 * @param string $body
	 * @param array $replyTo
	 * @param array $sender
	 * @return boolean
	 */
	protected function sendEmail(array $recipient, $subject, $body, array $replyTo = NULL, array $sender = NULL) {
		if ($sender === NULL) {
			$sender = array($this->settings['email']['adminSenderEmail'] => $this->settings['email']['adminName']);
		}
		$demoMode = isset($this->settings['email']['demoMode']) && $this->settings['email']['demoMode'] === TRUE;
		if ($demoMode) {
			$message = sprintf(
				'E-Mail :: subject: "%s", to: %s, from: %s (reply to: %s)',
				$subject,
				sprintf('%s <%s>', current($recipient), key($recipient)),
				sprintf('%s <%s>', current($sender), key($sender)),
				$replyTo === NULL ? 'not set' : sprintf('%s <%s>', current($replyTo), key($replyTo))
			);
			$this->systemLogger->log($message, LOG_DEBUG, array('body' => $body));
			return TRUE;
		} else {
			$mail = new \TYPO3\SwiftMailer\Message();
			$mail
				->setFrom($sender)
				->setTo($recipient)
				->setSubject($subject)
				->setBody($body);
			if ($replyTo !== NULL) {
				$mail->setReplyTo($replyTo);
			}
			return $mail->send();
		}
	}

	/**
	 * @param string $templatePathAndFilename
	 * @param array $variables
	 * @return string rendered template
	 */
	protected function renderFluidTemplate($templatePathAndFilename, array $variables = array()) {
		$templateSource = \TYPO3\FLOW3\Utility\Files::getFileContents($templatePathAndFilename, FILE_TEXT);
		$parsedTemplate = $this->templateParser->parse($templateSource);
		$renderingContext = new \TYPO3\Fluid\Core\Rendering\RenderingContext();
		$renderingContext->setControllerContext($this->controllerContext);
		$variableContainer = $renderingContext->getTemplateVariableContainer();
		foreach($variables as $name => $value) {
			$variableContainer->add($name, $value);
		}
		return $parsedTemplate->render($renderingContext);
	}
}
?>