<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Standard Controller
 */
class StandardController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\CircularRepository
	 */
	protected $circularRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectionRepository
	 */
	protected $electionRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorRepository
	 */
	protected $electorRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 */
	protected $electorateRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\NomineeRepository
	 */
	protected $nomineeRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\BudgetRepository
	 */
	protected $budgetRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\TokenRepository
	 */
	protected $tokenRepository;

	/**
	 * @param string $code
	 * @return void
	 */
	public function indexAction($code = NULL) {
		if ($code !== NULL) {
			if (strlen($code) < 5) {
				$this->addFlashMessage('Invalid token (code 1287152941)');
				$this->redirect('index');
			}
			$token = $this->tokenRepository->findOneByCode($code);
			if (!$token instanceof \TYPO3\BccVoting\Domain\Model\Token) {
				$this->addFlashMessage('Invalid token (code 1287152942)');
				$this->redirect('index');
			}
			$election = $token->getElection();
			$this->forward('show', 'Election', NULL, array('election' => $election, 'code' => $code));
		}
		$this->view->assign('circularCount', $this->circularRepository->countAll());
		$this->view->assign('electionCount', $this->electionRepository->countAll());
		$this->view->assign('electorCount', $this->electorRepository->countAll());
		$this->view->assign('electorateCount', $this->electorateRepository->countAll());
		$this->view->assign('nomineeCount', $this->nomineeRepository->countAll());
		$this->view->assign('budgetCount', $this->budgetRepository->countAll());
		// TODO $this->electionRepository->findByActive(TRUE) returns an empty result!
		$this->view->assign('activeElections', $this->electionRepository->findActive());
		$this->view->assign('expiredElections', $this->electionRepository->findExpired());
	}
}
?>