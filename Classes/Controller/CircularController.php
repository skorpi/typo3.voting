<?php
namespace TYPO3\BccVoting\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Circular Controller
 */
class CircularController extends \TYPO3\BccVoting\Controller\AbstractBaseController {

		/**
	 * Pattern to match a UUID link in the format [[uuid|title]]
	 */
	const PATTERN_TOKENLINK = '/\[\[(?P<uuid>([a-f0-9]){8}-([a-f0-9]){4}-([a-f0-9]){4}-([a-f0-9]){4}-([a-f0-9]){12})(\|?(?P<title>.*))\]\]/';

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\CircularRepository
	 */
	protected $circularRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 */
	protected $electorateRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectionRepository
	 */
	protected $electionRepository;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\TokenRepository
	 */
	protected $tokenRepository;

	/**
	 * @var \TYPO3\FLOW3\Persistence\PersistenceManagerInterface
	 */
	protected $persistenceManager;

	/**
	 * @param \TYPO3\FLOW3\Persistence\PersistenceManagerInterface $persistenceManager
	 */
	public function injectPersistenceManager(\TYPO3\FLOW3\Persistence\PersistenceManagerInterface $persistenceManager) {
		$this->persistenceManager = $persistenceManager;
	}

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('circulars', $this->circularRepository->findAll());
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $newCircular
	 * @return void
	 * @FLOW3\IgnoreValidation("$newCircular")
	 */
	public function newAction(\TYPO3\BccVoting\Domain\Model\Circular $newCircular = NULL) {
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('elections', $this->electionRepository->findAll());
		$this->view->assign('newCircular', $newCircular);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $newCircular
	 * @return void
	 */
	public function createAction(\TYPO3\BccVoting\Domain\Model\Circular $newCircular) {
		$this->circularRepository->add($newCircular);
		$this->addFlashMessage('Circular has been created', 'Circular created');
		$this->redirect('show', 'Circular', 'TYPO3.BccVoting', array('circular' => $newCircular));
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $circular
	 * @FLOW3\IgnoreValidation("$circular")
	 * @return void
	 */
	public function editAction(\TYPO3\BccVoting\Domain\Model\Circular $circular) {
		$this->view->assign('electorates', $this->electorateRepository->findAll());
		$this->view->assign('elections', $this->electionRepository->findAll());
		$this->view->assign('circular', $circular);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $circular
	 * @return void
	 */
	public function updateAction(\TYPO3\BccVoting\Domain\Model\Circular $circular) {
		$this->circularRepository->update($circular);
		$this->addFlashMessage('Circular has been updated', 'Circular updated');
		$this->redirect('show', 'Circular', 'TYPO3.BccVoting', array('circular' => $circular));
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $circular
	 * @return void
	 */
	public function deleteAction(\TYPO3\BccVoting\Domain\Model\Circular $circular) {
		$this->circularRepository->remove($circular);
		$this->addFlashMessage('Circular has been deleted', 'Circular deleted');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $circular
	 * @return void
	 */
	public function duplicateAction(\TYPO3\BccVoting\Domain\Model\Circular $circular) {
		// TODO $newCircular = clone $circular would not persist the cloned object...
		$newCircular = new \TYPO3\BccVoting\Domain\Model\Circular();
		$newCircular->setSubject($circular->getSubject() . ' - copy');
		$newCircular->setBody($circular->getBody());
		$newCircular->setElectorate($circular->getElectorate());
		$this->circularRepository->add($newCircular);
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $circular
	 * @return void
	 */
	public function showAction(\TYPO3\BccVoting\Domain\Model\Circular $circular) {
		$this->view->assign('circular', $circular);
		$tokenUriPattern = $this->settings['tokenUriPattern'];
		$electionRepository = $this->electionRepository;
		$replacedBody = preg_replace_callback(
			self::PATTERN_TOKENLINK,
			function($matches) use ($tokenUriPattern, $electionRepository) {
				// FIXME
				return str_replace('###CODE###', 'xxxxxx', $tokenUriPattern);
				/*
				$election = $electionRepository->findByUuid($matches['uuid']);
				return $uriBuilder
					->reset()
					->setCreateAbsoluteUri(TRUE)
					->uriFor('show', array('election' => $election, 'code' => 'xxxxxxxxx'), 'Election');
				 */
			},
			$circular->getBody()
		);
		$this->view->assign('replacedBody', $replacedBody);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $circular
	 * @return void
	 */
	public function sendAction(\TYPO3\BccVoting\Domain\Model\Circular $circular) {

		$maximumNumberOfCircularsAtOnce = (integer)$this->settings['maximumNumberOfCircularsAtOnce'];
		$recipients = $circular->getRemainingRecipients();
		$mailsSent = 0;
		foreach($recipients as $recipient) {
			$tokenUriPattern = $this->settings['tokenUriPattern'];
			$electionRepository = $this->electionRepository;
			$tokenRepository = $this->tokenRepository;
			$replacedBody = preg_replace_callback(
				self::PATTERN_TOKENLINK,
				function($matches) use ($tokenUriPattern, $electionRepository, $tokenRepository) {
					$election = $electionRepository->findByIdentifier($matches['uuid']);
					$code = md5(mt_rand());
					$token = new \TYPO3\BccVoting\Domain\Model\Token($code, new \DateTime(), $election);
					$tokenRepository->add($token);
					// FIXME
					return str_replace('###CODE###', $code, $tokenUriPattern);
					/*
					return $uriBuilder
						->reset()
						->setCreateAbsoluteUri(TRUE)
						->uriFor('show', array('election' => $election, 'code' => $code), 'Election');
					 */
				},
				$circular->getBody()
			);
			$replacedBody = str_replace('###fullName###', $recipient->getFullName(), $replacedBody);
			$replacedBody = str_replace('###firstName###', $recipient->getFirstName(), $replacedBody);
			$replacedBody = str_replace('###email###', $recipient->getEmail(), $replacedBody);
			$this->sendEmail(array($recipient->getEmail() => $recipient->getFullName()), $circular->getSubject(), $replacedBody);
			$mailsSent ++;
			$circular->addServedRecipient($recipient);
			$this->circularRepository->update($circular);
			$this->persistenceManager->persistAll();
			if ($mailsSent >= $maximumNumberOfCircularsAtOnce) {
				//$this->redirect('send', NULL, NULL, array('circular' => $circular), 5);
				$this->view->assign('circular', $circular);
				return;
			}
		}
		$this->addFlashMessage('All circulars have been sent!', 'Circulars sent');
		$this->redirect('index');
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Circular $circular
	 * @param string $email
	 * @return void
	 */
	public function sendPreviewAction(\TYPO3\BccVoting\Domain\Model\Circular $circular, $email) {
		$tokenUriPattern = $this->settings['tokenUriPattern'];
		$replacedBody = preg_replace_callback(
			self::PATTERN_TOKENLINK,
			function($matches) use ($tokenUriPattern) {
				// FIXME
				return str_replace('###CODE###', 'xxxxxx', $tokenUriPattern);
				/*
				$election = $electionRepository->findByUuid($matches['uuid']);
				return $uriBuilder
					->reset()
					->setCreateAbsoluteUri(TRUE)
					->uriFor('show', array('election' => $election, 'code' => 'xxxxxxxxx'), 'Election');
				 */
			},
			$circular->getBody()
		);
		$replacedBody = str_replace('###fullName###', 'John Doe', $replacedBody);
		$replacedBody = str_replace('###firstName###', 'John', $replacedBody);
		$replacedBody = str_replace('###email###', 'john.doe@test.com', $replacedBody);
		$this->sendEmail(array($email => 'Test Recipient'), $circular->getSubject(), $replacedBody);
		$this->addFlashMessage('Test mail has been sent', 'Test mail sent');
		$this->redirect('show', 'Circular', 'TYPO3.BccVoting', array('circular' => $circular));
	}
}
?>