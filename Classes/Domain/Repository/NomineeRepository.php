<?php
namespace TYPO3\BccVoting\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * The Nominee Repository
 *
 * @FLOW3\Scope("singleton")
 */
class NomineeRepository extends \TYPO3\FLOW3\Persistence\Repository {

	/**
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectionRepository
	 * @FLOW3\Inject
	 */
	protected $electionRepository;

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $nominee
	 * @return void
	 */
	public function remove($nominee) {
			// TODO this should be done automatically somehow but cascade annotations did not work?
		$nominee->removeImage();
		$affectedElections = $this->electionRepository->findByNominee($nominee);
		foreach($affectedElections as $election) {
			$this->electionRepository->remove($election);
		}
		parent::remove($nominee);
	}

}

?>