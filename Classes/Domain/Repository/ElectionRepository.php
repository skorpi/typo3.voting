<?php
namespace TYPO3\BccVoting\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * The Election Repository
 *
 * @FLOW3\Scope("singleton")
 */
class ElectionRepository extends \TYPO3\FLOW3\Persistence\Repository {

	/**
	 * @var \TYPO3\BccVoting\Domain\Repository\TokenRepository
	 * @FLOW3\Inject
	 */
	protected $tokenRepository;

	/**
	 * @var \TYPO3\BccVoting\Domain\Repository\VoteRepository
	 * @FLOW3\Inject
	 */
	protected $voteRepository;

	/**
	 * @param  $object
	 * @return void
	 */
	public function remove($object) {
		$affectedTokens = $this->tokenRepository->findByElection($object);
		foreach($affectedTokens as $token) {
			$this->tokenRepository->remove($token);
		}

		$affectedVotes = $this->voteRepository->findByElection($object);
		foreach($affectedVotes as $vote) {
			$this->voteRepository->remove($vote);
		}
		parent::remove($object);
	}

	/**
	 * @return \TYPO3\FLOW3\Persistence\QueryResultProxy
	 */
	public function findAll() {
		// TODO this is required to avoid exception 'undefined index "startDate"' when there are no elections yet
		if ($this->countAll() === 0) {
			return array();
		}
		return $this->createQuery()
			->setOrderings(
					array(
						'startDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING,
						'endDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING
					)
				)
			->execute();
	}

	/**
	 * @return \TYPO3\FLOW3\Persistence\QueryResultProxy
	 * TODO: is it possible to replace constraint by $query->equals('active', TRUE)?
	 */
	public function findActive() {
		// TODO this is required to avoid exception 'undefined index "startDate"' when there are no elections yet
		if ($this->countAll() === 0) {
			return array();
		}
		$now = new \DateTime();
		$query = $this->createQuery();
		return $query
			->matching(
				$query->logicalAnd(
					$query->lessThanOrEqual('startDate', $now),
					$query->greaterThanOrEqual('endDate', $now)
				)
			)
			->setOrderings(
					array(
						'startDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING,
						'endDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING
					)
				)
			->execute();
	}

	/**
	 * @return \TYPO3\FLOW3\Persistence\QueryResultProxy
	 * TODO: is it possible to replace constraint by $query->equals('active', TRUE)?
	 */
	public function findExpired() {
		// TODO this is required to avoid exception 'undefined index "startDate"' when there are no elections yet
		if ($this->countAll() === 0) {
			return array();
		}
		$now = new \DateTime();
		$query = $this->createQuery();
		return $query
			->matching(
				$query->lessThan('endDate', $now)
			)
			->setOrderings(
					array(
						'startDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING,
						'endDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING
					)
				)
			->execute();
	}

	/**
	 * @return \TYPO3\FLOW3\Persistence\QueryResultProxy
	 * TODO: is it possible to replace constraint by $query->equals('active', TRUE)?
	 */
	public function findUpcoming() {
		// TODO this is required to avoid exception 'undefined index "startDate"' when there are no elections yet
		if ($this->countAll() === 0) {
			return array();
		}
		$now = new \DateTime();
		$query = $this->createQuery();
		return $query
			->matching(
				$query->greaterThan('startDate', $now)
			)
			->setOrderings(
					array(
						'startDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING,
						'endDate' => \TYPO3\FLOW3\Persistence\QueryInterface::ORDER_ASCENDING
					)
				)
			->execute();
	}
}

?>