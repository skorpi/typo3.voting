<?php
namespace TYPO3\BccVoting\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * The Electorate Repository
 *
 * @FLOW3\Scope("singleton")
 */
class ElectorateRepository extends \TYPO3\FLOW3\Persistence\Repository {

	/**
	 * @var \TYPO3\BccVoting\Domain\Repository\CircularRepository
	 * @FLOW3\Inject
	 */
	protected $circularRepository;

	/**
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectionRepository
	 * @FLOW3\Inject
	 */
	protected $electionRepository;

	/**
	 * @param  $object
	 * @return void
	 */
	public function remove($object) {
		$affectedCirculars = $this->circularRepository->findByElectorate($object);
		foreach($affectedCirculars as $circular) {
			$this->circularRepository->remove($circular);
		}
		$affectedElections = $this->electionRepository->findByElectorate($object);
		foreach($affectedElections as $election) {
			$this->electionRepository->remove($election);
		}
		parent::remove($object);
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @return \TYPO3\FLOW3\Persistence\QueryResultInterface
	 */
	public function findByElector(\TYPO3\BccVoting\Domain\Model\Elector $elector) {
		$query = $this->createQuery();
		return $query
			->matching(
				$query->contains('electors', $elector)
			)
			->execute();
	}
}

?>