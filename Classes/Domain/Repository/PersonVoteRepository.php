<?php
namespace TYPO3\BccVoting\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * The Vote Repository
 *
 * @FLOW3\Scope("singleton")
 */
class PersonVoteRepository extends \TYPO3\FLOW3\Persistence\Repository {

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election election to get the voting results for
	 * @return array
	 * todo instead of an array this should return a dedicated model instance
	 */
	public function getVotingResult(\TYPO3\BccVoting\Domain\Model\Election $election) {
		$votes = $this->findByElection($election);
		$nominees = $election->getNominees();
		$nomineeVotes = array();
		$totalVotes = 0;
		foreach($nominees as $nominee) {
			$nomineeVote = array('nominee' => $nominee, 'votes' => 0);
			foreach($votes as $vote) {
				if ($vote->getNominees()->contains($nominee)) {
					$nomineeVote['votes'] ++;
					$totalVotes ++;
				}
			}
			$nomineeVotes[] = $nomineeVote;
		}
		usort($nomineeVotes, function ($nomineeVote1, $nomineeVote2) {
			if ($nomineeVote1['votes'] === $nomineeVote2['votes']) {
				return strcmp($nomineeVote1['nominee']->getLastName(), $nomineeVote2['nominee']->getLastName());
			}
			return $nomineeVote1['votes'] < $nomineeVote2['votes'];
		});
		$votingResult = array(
			'totalVotes' => $totalVotes,
			'nomineeVotes' => $nomineeVotes,
		);
		return $votingResult;
	}
}

?>