<?php
namespace TYPO3\BccVoting\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.BccVoting".            *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A repository for Budgets
 *
 * @FLOW3\Scope("singleton")
 */
class BudgetRepository extends \TYPO3\FLOW3\Persistence\Repository {

	// add customized methods here

}
?>