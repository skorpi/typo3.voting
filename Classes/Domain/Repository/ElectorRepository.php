<?php
namespace TYPO3\BccVoting\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * The Elector Repository
 *
 * @FLOW3\Scope("singleton")s
 */
class ElectorRepository extends \TYPO3\FLOW3\Persistence\Repository {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 */
	protected $electorateRepository;

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @return void
	 * TODO is this required?
	 */
	public function remove($elector) {
		foreach($elector->getElectorates() as $electorate) {
			$electorate->removeElector($elector);
			$this->electorateRepository->update($electorate);
		}
		parent::remove($elector);
	}

	/**
	 * @param \SplObjectStorage<\TYPO3\BccVoting\Domain\Model\Elector> $electors
	 * @return \SplObjectStorage<\TYPO3\BccVoting\Domain\Model\Elector>
	 * TODO: refactor
	 */
//	public function findRemaining(\SplObjectStorage $electors) {
//		$emails = array();
//		foreach($electors as $elector) {
//			$emails[] = $elector->getEmail();
//		}
//		$query = $this->createQuery();
//		if (count($emails) > 0) {
//			$query = $query->matching(
//				$query->logicalNot(
//					$query->in('email', $emails)
//				)
//			);
//		}
//		return $query->execute();
//	}
}

?>