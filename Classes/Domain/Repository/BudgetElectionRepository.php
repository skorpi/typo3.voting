<?php
namespace TYPO3\BccVoting\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * The Election Repository
 *
 * @FLOW3\Scope("singleton")
 */
class BudgetElectionRepository extends ElectionRepository {

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $buget
	 * @return \TYPO3\FLOW3\Persistence\QueryResultInterface
	 */
	public function findByBudget(\TYPO3\BccVoting\Domain\Model\Budget $budget) {
		$query = $this->createQuery();
		return $query
			->matching(
			$query->contains('budgets', $budget)
		)
			->execute();
	}

}

?>