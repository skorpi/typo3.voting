<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Elector
 *
 * @FLOW3\Entity
 */
class Elector extends \TYPO3\BccVoting\Domain\Model\Person {

	/**
	 * @var \TYPO3\BccVoting\Domain\Repository\ElectorateRepository
	 * @FLOW3\Inject
	 */
	protected $electorateRepository;

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Electorate>
	 */
	public function getElectorates() {
		return $this->electorateRepository->findByElector($this);
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->getFullName() . ' (' . $this->getEmail() . ')';
	}
}
?>