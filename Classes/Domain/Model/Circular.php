<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Circular
 *
 * @FLOW3\Entity
 */
class Circular {

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="StringLength", options={ "minimum"=1, "maximum"=100 })
	 * @FLOW3\Identity
	 */
	protected $subject = '';

	/**
	 * @var string
	 * @FLOW3\Validate(type="StringLength", options={ "minimum"=1 })
	 * @ORM\Column(type="text")
	 */
	protected $body = '';

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\Electorate
	 * @ORM\ManyToOne
	 */
	protected $electorate;

	/**
	 * @var array
	 */
	protected $servedEmails = array();

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->servedRecipients = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @param string $subject
	 * @return void
	 */
	public function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
	 * @return string
	 */
	public function getSubject() {
		return $this->subject;
	}

	/**
	 * @param string $body
	 * @return void
	 */
	public function setBody($body) {
		$this->body = $body;
	}

	/**
	 * @return string
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 */
	public function setElectorate(\TYPO3\BccVoting\Domain\Model\Electorate $electorate) {
		$this->electorate = $electorate;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Electorate
	 */
	public function getElectorate() {
		return $this->electorate;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @return void
	 */
	public function addServedRecipient(\TYPO3\BccVoting\Domain\Model\Elector $elector) {
		$this->servedEmails[$elector->getEmail()] = $elector->getFullName() . ' (' . $elector->getEmail() . ')';
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Elector>
	 */
	public function getServedEmails() {
		return $this->servedEmails;
	}

	/**
	 * @return \SplObjectStorage<\TYPO3\BccVoting\Domain\Model\Elector>
	 */
	public function getRemainingRecipients() {
		$remainingRecipients = new \SplObjectStorage();
		foreach($this->electorate->getElectors() as $elector) {
			if (isset($this->servedEmails[$elector->getEmail()])) {
				continue;
			}
			$remainingRecipients->attach($elector);
		}
		return $remainingRecipients;
	}
}
?>