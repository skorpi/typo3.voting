<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * An image
 *
 * @FLOW3\Entity
 */
class Image {

	/**
	 * @var string
	 * @FLOW3\Validate(type="StringLength", options={ "maximum"=255 })
	 */
	protected $title;

	/**
	 * @var \TYPO3\FLOW3\Resource\Resource
	 * @ORM\ManyToOne(cascade={"persist", "remove"})
	 * @FLOW3\Validate(type="NotEmpty")
	 */
	protected $originalResource;

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 * @author Robert Lemke <robert@typo3.org>
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Gets the title
	 *
	 * @return string The title
	 * @author Robert Lemke <robert@typo3.org>
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the original resource
	 *
	 * @param \TYPO3\FLOW3\Resource\Resource $originalResource
	 * @return void
	 */
	public function setOriginalResource(\TYPO3\FLOW3\Resource\Resource $originalResource) {
		$this->originalResource = $originalResource;
	}

	/**
	 * Returns the original resource
	 *
	 * @return \TYPO3\FLOW3\Resource\Resource $originalResource
	 */
	public function getOriginalResource() {
		return $this->originalResource;
	}
}

?>