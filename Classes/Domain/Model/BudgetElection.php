<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Budget Election
 *
 * @FLOW3\Entity
 */
class BudgetElection extends Election {

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Budget>
	 * @ORM\ManyToMany(cascade={"persist"})
	 */
	protected $budgets;

	/**
	 * @var string
	 * @FLOW3\Transient
	 */
	protected $type = 'Budget';

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->budgets = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Budget> $budgets
	 * @return void
	 */
	public function setBudgets(\Doctrine\Common\Collections\ArrayCollection $budgets) {
		$this->budgets = $budgets;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Budget>
	 */
	public function getBudgets() {
		return $this->budgets;
	}


	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $budget
	 * @return void
	 */
	public function addBudget(\TYPO3\BccVoting\Domain\Model\Budget $budget) {
		$this->budgets->attach($budget);
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

}
?>