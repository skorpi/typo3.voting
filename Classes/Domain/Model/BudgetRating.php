<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * @FLOW3\Entity
 */
class BudgetRating {

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\Budget
	 * @ORM\ManyToOne
	 */
	protected $budget;

	/**
	 * @var integer
	 */
	protected $rating;

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\BudgetVote
	 * @ORM\ManyToOne(inversedBy="budgetRatings")
	 */
	protected $budgetVote;

	/**
	* @param \TYPO3\BccVoting\Domain\Model\Budget $budget
	*/
	public function setBudget(\TYPO3\BccVoting\Domain\Model\Budget $budget) {
		$this->budget = $budget;
	}

	/**
	* @return \TYPO3\BccVoting\Domain\Model\Budget
	*/
	public function getBudget() {
		return $this->budget;
	}

	/**
	* @param int $rating
	*/
	public function setRating($rating) {
		$this->rating = $rating;
	}

	/**
	* @return int
	*/
	public function getRating() {
		return $this->rating;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\BudgetVote $budgetVote
	 */
	public function setBudgetVote(\TYPO3\BccVoting\Domain\Model\BudgetVote $budgetVote) {
		$this->budgetVote = $budgetVote;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\BudgetVote
	 */
	public function getBudgetVote() {
		return $this->budgetVote;
	}


}
