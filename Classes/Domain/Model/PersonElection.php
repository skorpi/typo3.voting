<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Election
 *
 * @FLOW3\Entity
 */
class PersonElection extends Election {

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Nominee>
	 * @ORM\ManyToMany(cascade={"persist"})
	 */
	protected $nominees;

	/**
	 * @var integer
	 */
	protected $maxNumberOfVotes = 0;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->nominees = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Nominee> $nominees
	 * @return void
	 */
	public function setNominees(\Doctrine\Common\Collections\ArrayCollection $nominees) {
		$this->nominees = $nominees;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Nominee>
	 */
	public function getNominees() {
		return $this->nominees;
	}

	/**
	 * @return \SplObjectStorage<\TYPO3\BccVoting\Domain\Model\Nominee>
	 */
	public function getPublicNominees() {
		$publicNominees = new \SplObjectStorage();
		foreach($this->nominees as $nominee) {
			if ($nominee->isPublic() === TRUE) {
				$publicNominees->attach($nominee);
			}
		}
		return $publicNominees;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Nominee $nominee
	 * @return void
	 */
	public function addNominee(\TYPO3\BccVoting\Domain\Model\Nominee $nominee) {
		$this->nominees->attach($nominee);
	}

	/**
	 * @param integer $maxNumberOfVotes
	 * @return void
	 */
	public function setMaxNumberOfVotes($maxNumberOfVotes) {
		$this->maxNumberOfVotes = (integer)$maxNumberOfVotes;
	}

	/**
	 * @return integer
	 */
	public function getMaxNumberOfVotes() {
		return $this->maxNumberOfVotes;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return 'Person';
	}
}
?>