<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Vote
 *
 * @FLOW3\Entity
 */
class BudgetVote extends Vote {

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\BudgetRating>
	 * @ORM\OneToMany(mappedBy="budgetVote")
	 */
	protected $budgetRatings;

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->budgetRatings = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\BudgetRating> $budgetRatings
	 */
	public function setBudgetRatings(\Doctrine\Common\Collections\ArrayCollection $budgetRatings) {
		$this->budgetRatings = $budgetRatings;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\BudgetRating>
	 */
	public function getBudgetRatings() {
		return $this->budgetRatings;
	}

}
?>