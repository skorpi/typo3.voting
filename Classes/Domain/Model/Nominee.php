<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Nominee
 *
 * @FLOW3\Entity
 */
class Nominee extends \TYPO3\BccVoting\Domain\Model\Person {

	/**
	 * @var \TYPO3\BccVoting\Domain\Repository\PersonElectionRepository
	 * @FLOW3\Inject
	 */
	protected $personElectionRepository;

	/**
	 * @var string
	 */
	protected $url = '';

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $comments = '';

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\Image
	 * @ORM\ManyToOne(cascade={"persist", "remove"})
	 */
	protected $image;

	/**
	 * @var boolean
	 */
	protected $public = FALSE;

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\PersonElection>
	 */
	public function getElections() {
		return $this->personElectionRepository->findByNominee($this);
	}

	/**
	 * @param string $url
	 * @return void
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @param string $comments
	 * @return void
	 */
	public function setComments($comments) {
		$this->comments = $comments;
	}

	/**
	 * @return string
	 */
	public function getComments() {
		return $this->comments;
	}

	/**
	 * @return TYPO3\BccVoting\Domain\Model\Image
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Image $image
	 */
	public function setImage(\TYPO3\BccVoting\Domain\Model\Image $image) {
		$this->image = $image;
	}

	/**
	 * @return void
	 */
	public function removeImage() {
		$this->image = NULL;
	}

	/**
	 * @param boolean $isPublic
	 * @return void
	 */
	public function setPublic($isPublic) {
		$this->public = $isPublic;
	}

	/**
	 * @return boolean
	 */
	public function isPublic() {
		return $this->public;
	}

	/**
	 * Alias for self::isPublic()
	 * @return boolean
	 * TODO is this required for QOM?
	 */
	public function getPublic() {
		return $this->public;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->getFullName() . ' (' . $this->getEmail() . ')';
	}

}
?>