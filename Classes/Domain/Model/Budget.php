<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.BccVoting".            *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;

/**
 * A Budget
 *
 * @FLOW3\Entity
 */
class Budget {

	/**
	 * The title
	 * @var string
	 */
	protected $title;

	/**
	 * The description
	 * @var string
	 * @FLOW3\Validate(type="Raw")
	 * @ORM\Column(type="text")
	 */
	protected $description;

	/**
	 * The applicant
	 * @var string
	 */
	protected $applicant;

	/**
	 * The url
	 * @var string
	 */
	protected $url;

	/**
	 * The costs
	 * @var integer
	 */
	protected $amount;

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\BudgetElectionRepository
	 */
	protected $budgetElectionRepository;

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\BudgetElection>
	 */
	public function getElections(){
		return $this->budgetElectionRepository->findByBudget($this);
	}

	/**
	 * Get the Budget's title
	 *
	 * @return string The Budget's title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets this Budget's title
	 *
	 * @param string $title The Budget's title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Get the Budget's description
	 *
	 * @return string The Budget's description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets this Budget's description
	 *
	 * @param string $description The Budget's description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @param int $amount
	 */
	public function setAmount($amount) {
		$this->amount = $amount;
	}

	/**
	 * @return int
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 * @param string $applicant
	 */
	public function setApplicant($applicant) {
		$this->applicant = $applicant;
	}

	/**
	 * @return string
	 */
	public function getApplicant() {
		return $this->applicant;
	}

	/**
	 * @param string $url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->getTitle() . ' (' . $this->getAmount() . ' €)';
	}

	/**
	 * @return null|string
	 */
	public function getIdentity(){
		return $this->FLOW3_Persistence_Identifier;
	}
}
?>