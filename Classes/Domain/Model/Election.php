<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Election
 *
 * @FLOW3\Entity
 * @ORM\InheritanceType("JOINED")
 */
class Election {

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="StringLength", options={ "minimum"=1, "maximum"=100 })
	 * FLOW3\Identity
	 */
	protected $title = '';

	/**
	 * @var \DateTime
	 * FLOW3\Identity
	 */
	protected $startDate;

	/**
	 * @var \DateTime
	 */
	protected $endDate;

	/**
	 * @var string
	 * @FLOW3\Validate(type="Raw")
	 * @ORM\Column(type="text")
	 */
	protected $description;

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\Electorate
	 * @ORM\ManyToOne
	 */
	protected $electorate;


	/**
	 * Constructor
	 */
	public function __construct() {
		$this->startDate = new \DateTime();
		$this->endDate = new \DateTime();
	}

	/**
	 * Returns TRUE if this election is active (startDate >= NOW && endDate <= NOW)
	 * @return boolean
	 */
	public function isActive() {
		$now = new \DateTime();
		return $this->startDate <= $now && $this->endDate >= $now;
	}

	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param \DateTime $startDate
	 * @return void
	 */
	public function setStartDate(\DateTime $startDate) {
		$this->startDate = $startDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getStartDate() {
		return $this->startDate;
	}

	/**
	 * @param \DateTime $endDate
	 * @return void
	 */
	public function setEndDate(\DateTime $endDate) {
		$this->endDate = $endDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getEndDate() {
		return $this->endDate;
	}

	/**
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 * @return void
	 */
	public function setElectorate($electorate) {
		$this->electorate = $electorate;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Electorate
	 */
	public function getElectorate() {
		return $this->electorate;
	}

}
?>