<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Token
 *
 * @FLOW3\Entity
 */
class Token {

	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @var \DateTime
	 */
	protected $date;

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\Election
	 * @ORM\ManyToOne
	 */
	protected $election;

	/**
	 * Constructor
	 *
	 * @param string $code
	 * @param \DateTime $date
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 */
	public function __construct($code, \DateTime $date, \TYPO3\BccVoting\Domain\Model\Election $election) {
		$this->code = $code;
		$this->date = $date;
		$this->election = $election;
	}

	/**
	 * @param string $code
	 * @return void
	 */
	public function setCode($code) {
		$this->code = $code;
	}

	/**
	 * @return string
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @param \DateTime $date
	 * @return void
	 */
	public function setDate(\DateTime $date) {
		$this->date = $date;
	}

	/**
	 * @return string
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 */
	public function setElection(\TYPO3\BccVoting\Domain\Model\Election $election) {
		$this->election = $election;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Election
	 */
	public function getElection() {
		return $this->election;
	}
}
?>