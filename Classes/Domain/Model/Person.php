<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Person base class
 *
 * @FLOW3\Entity
 * @ORM\InheritanceType("JOINED")
 */
class Person {

	const GENDER_MALE = 'male';
	const GENDER_FEMALE = 'female';

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="NotEmpty")
	 */
	protected $gender;

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="StringLength", options={ "minimum"=1, "maximum"=100 })
	 */
	protected $firstName = '';

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="StringLength", options={ "minimum"=0, "maximum"=100 })
	 */
	protected $middleName = '';

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="StringLength", options={ "minimum"=1, "maximum"=100 })
	 */
	protected $lastName = '';

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="EmailAddress")
	 * FLOW3\Identity
	 */
	protected $email = '';

	/**
	 * @param string $gender Gender of this Nominee (one of GENDER_* constants)
	 * @return void
	 */
	public function setGender($gender) {
		$this->gender = ($gender === self::GENDER_FEMALE) ? $gender : self::GENDER_MALE;
	}

	/**
	 * @return string Gender of this Nominee (one of GENDER_* constants)
	 */
	public function getGender() {
		return $this->gender;
	}

	/**
	 * @param string $firstName
	 * @return void
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param string $middleName
	 * @return void
	 */
	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
	}

	/**
	 * @return string
	 */
	public function getMiddleName() {
		return $this->middleName;
	}

	/**
	 * @param string $lastName
	 * @return void
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @return string
	 */
	public function getFullName() {
		$fullName = $this->getFirstName();
		if ($this->getMiddleName() !== '') {
			$fullName .= ' ' . $this->getMiddleName();
		}
		$fullName .= ' ' . $this->getLastName();
		return $fullName;
	}

	/**
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

}
?>