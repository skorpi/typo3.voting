<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A Electorate
 *
 * @FLOW3\Entity
 */
class Electorate {

	/**
	 * @var string
	 	 * @FLOW3\Validate(type="StringLength", options={ "minimum"=1, "maximum"=100 })
	 * @FLOW3\Identity
	 */
	protected $title = '';

	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Elector>
	 * @ORM\ManyToMany(cascade={"persist"})
	 * // TODO: add lazy annotation (does not work with CSV import currently)
	 */
	protected $electors;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->electors = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @return void
	 */
	public function addElector(\TYPO3\BccVoting\Domain\Model\Elector $elector) {
		if (!$this->electors->contains($elector)) {
			$this->electors->add($elector);
		}
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Elector $elector
	 * @return void
	 */
	public function removeElector(\TYPO3\BccVoting\Domain\Model\Elector $elector) {
		$this->electors->removeElement($elector);
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Elector> $electors
	 * @return void
	 */
	public function setElectors(\Doctrine\Common\Collections\ArrayCollection $electors) {
		$this->electors = $electors;
		// TODO what this the way to implement n:n relations?
		/*
		foreach($this->electors as $elector) {
			$this->removeElector($elector);
		}
		foreach($electors as $elector) {
			$this->addElector($elector);
		}
		*/
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection<\TYPO3\BccVoting\Domain\Model\Elector>
	 */
	public function getElectors() {
		return $this->electors;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->getTitle() . ' (' . count($this->getElectors()) . ')';
	}
}
?>