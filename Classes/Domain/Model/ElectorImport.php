<?php
namespace TYPO3\BccVoting\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A ElectorImport
 *
 * @FLOW3\Entity
 */
class ElectorImport {

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\Electorate
	 * @ORM\ManyToOne
	 */
	protected $electorate;

	/**
	 * @var \TYPO3\FLOW3\Resource\Resource
	 * @ORM\ManyToOne
	 */
	protected $csvResource;

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 * @return void
	 */
	public function setElectorate(\TYPO3\BccVoting\Domain\Model\Electorate $electorate) {
		$this->electorate = $electorate;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Electorate $electorate
	 */
	public function getElectorate() {
		return $this->electorate;
	}

	/**
	 * @param \TYPO3\FLOW3\Resource\Resource $csvResource
	 * @return void
	 */
	public function setCsvResource(\TYPO3\FLOW3\Resource\Resource $csvResource) {
		$this->csvResource = $csvResource;
	}

	/**
	 * @return \TYPO3\FLOW3\Resource\Resource $csvResource
	 */
	public function getCsvResource() {
		return $this->csvResource;
	}

}
?>