<?php
namespace TYPO3\BccVoting\ViewHelpers\Form;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * @api
 * @FLOW3\Scope("prototype")
 */
class IfHasErrorsViewHelper extends \TYPO3\Fluid\Core\ViewHelper\AbstractConditionViewHelper {

	/**
	 * @return string the rendered string
	 */
	public function render() {
		$errors = $this->controllerContext->getRequest()->getErrors();
		if (count($errors) > 0) {
			return $this->renderThenChild();
		} else {
			return $this->renderElseChild();
		}
	}

}

?>
