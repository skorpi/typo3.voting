<?php
namespace TYPO3\BccVoting\ViewHelpers\Form;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use Doctrine\ORM\Mapping as ORM;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 *
 * @api
 * @FLOW3\Scope("prototype")
 */
class DateTimeViewHelper extends \TYPO3\Fluid\ViewHelpers\Form\AbstractFormFieldViewHelper {

	/**
	 * @var string
	 */
	protected $tagName = 'input';

	/**
	 * Initialize the arguments.
	 *
	 * @return void

	 * @api
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerTagAttribute('size', 'int', 'The size of the input field');
		$this->registerArgument('errorClass', 'string', 'CSS class to set if there are errors for this view helper', FALSE, 'f3-form-error');
		$this->registerUniversalTagAttributes();
	}

	/**
	 * Renders the textbox, hiddenfield and required javascript
	 *
	 * @param string $dateFormat
	 * @param string $initialDate
	 * @return string
	 */
	public function render($dateFormat = 'Y-m-d', $initialDate = 'today') {
		$name = $this->getName();
		$this->registerFieldNameForFormTokenGeneration($name);

		$this->tag->addAttribute('type', 'text');
		$this->tag->addAttribute('name', $name . '[date]');
		$this->tag->addAttribute('readonly', TRUE);
		$date = $this->getValue();
		if (is_string($date) && strlen($date) > 0) {
			$date = \DateTime::createFromFormat(\DateTime::W3C, $date);
		} elseif (!$date instanceof \DateTime) {
			$date = new \DateTime($initialDate);
		}

		$this->tag->addAttribute('value', $date->format($dateFormat));

		if ($this->hasArgument('id')) {
			$id = $this->arguments['id'];
		} else {
			$id = 'field' . md5(uniqid());
			$this->tag->addAttribute('id', $id);
		}
		$content = '';
		$content .= $this->tag->render();


		$content .= $this->buildHourSelector($date);
		$content .= $this->buildMinuteSelector($date);
		$content .= '<input type="hidden" name="' . $name . '[dateFormat]" value="' . htmlspecialchars($dateFormat) . '" />';
		$datePickerDateFormat = $this->convertDateFormatToDatePickerFormat($dateFormat);

		$content .= '<script type="text/javascript">//<![CDATA[
			$(function() {
				$("#' . $id . '").datepicker({
					dateFormat: "' . $datePickerDateFormat . '",
				});
			});
			//]]></script>';
		return $content;
	}

	protected function buildHourSelector(\DateTime $date) {
		$value = $date->format('H');
		$fieldName = sprintf('%s[hour]', $this->getName());
		$selector = '<select name="' . htmlspecialchars($fieldName) . '">';
		foreach(range(0, 23) as $hour) {
			$hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
			$selected = $hour === $value ? ' selected="selected"' : '';
			$selector .= '<option value="' . $hour . '"'.$selected.'>' . $hour . '</option>';
		}
		$selector .= '</select>';
		return $selector;
	}

	protected function buildMinuteSelector(\DateTime $date) {
		$value = $date->format('i');
		$fieldName = sprintf('%s[minute]', $this->getName());
		$selector = '<select name="' . htmlspecialchars($fieldName) . '">';
		foreach(range(0, 59) as $minute) {
			$minute = str_pad($minute, 2, '0', STR_PAD_LEFT);
			$selected = $minute === $value ? ' selected="selected"' : '';
			$selector .= '<option value="' . $minute . '"'.$selected.'>' . $minute . '</option>';
		}
		$selector .= '</select>';
		return $selector;
	}

	/**
	 * @param string $dateFormat
	 * @return string
	 */
	protected function convertDateFormatToDatePickerFormat($dateFormat) {
		$replacements = array(
			'd' => 'dd',
			'D' => 'D',
			'j' => 'o',
			'l' => 'DD',

			'F' => 'MM',
			'm' => 'mm',
			'M' => 'M',
			'n' => 'm',

			'Y' => 'yy',
			'y' => 'y'
		);
		return strtr($dateFormat, $replacements);
	}
}

?>