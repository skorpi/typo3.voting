<?php
namespace TYPO3\BccVoting\ViewHelpers\Identity;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

/**
 * Renders the UUID of a persisted object
 *
 * = Examples =
 *
 * <code>
 * <f:identity.uuid object="{post.blog}" />
 * </code>
 * <output>
 * 97e7e90a-413c-44ef-b2d0-ddfa4387b5ca
 * </output>
 *
 * @api
 */
class UuidViewHelper extends \TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @var \TYPO3\FLOW3\Persistence\PersistenceManagerInterface
	 */
	protected $persistenceManager;

	/**
	 * Injects the FLOW3 Persistence Manager
	 *
	 * @param \TYPO3\FLOW3\Persistence\PersistenceManagerInterface $persistenceManager
	 * @return void
	 */
	public function injectPersistenceManager(\TYPO3\FLOW3\Persistence\PersistenceManagerInterface $persistenceManager) {
		$this->persistenceManager = $persistenceManager;
	}

	/**
	 * Renders the output of this view helper
	 *
	 * @param object $object The persisted object
	 * @return string Identity

	 * @api
	 */
	public function render($object = NULL) {
		if ($object === NULL) {
			$object = $this->renderChildren();
		}
		if (!is_object($object)) {
			throw new \TYPO3\Fluid\Exception('f:identity.uuid expects an object, ' . \gettype($object) . ' given.', 1283947752);
		}
		return $this->persistenceManager->getIdentifierByObject($object);
	}
}

?>