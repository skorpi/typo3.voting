<?php
namespace TYPO3\BccVoting\ViewHelpers;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

/**
 * Renders a percent bar..
 */
class PercentBarViewHelper extends \TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Renders the output of this view helper
	 *
	 * @param integer $currentValue
	 * @param integer $targetValue
	 * @return string Percent bar
	 */
	public function render($currentValue, $targetValue) {
		$percentage = ceil($currentValue / $targetValue * 100);
		return '<div class="percentBar"><div style="width: ' . $percentage . '%"></div></div>';
	}

}

?>