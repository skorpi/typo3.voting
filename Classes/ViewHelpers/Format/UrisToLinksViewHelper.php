<?php
namespace TYPO3\BccVoting\ViewHelpers\Format;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

/**
 * @api
 */
class UrisToLinksViewHelper extends \TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Pattern to match an URI like http://www.foo.bar
	 */
	const PATTERN_URIS = '@(?<![.*">])\b(?:(?:https?|ftp|file)://|[a-z]\.)[-A-Z0-9+&#/%=~_|$?!:,.]*[A-Z0-9+&#/%=~_|$]@i';

	/**
	 * @return string string with URIs replaced by corresponding links
	 * @api
	 */
	public function render() {
		$text = $this->renderChildren();
		return preg_replace(self::PATTERN_URIS, '<a href="\0" onclick="alert(this.href); return false">\0</a>', $text);
	}


}

?>