<?php
namespace TYPO3\BccVoting\Service;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * CSV Service
 * @FLOW3\Scope("singleton")
 */
class CsvService {

	/**
	 * Pattern to match one line of a CSV file
	 */
	const PATTERN_LINE = '/(?<=^|[,;])(((?:")(?P<quotedValue>[^"]*)(?:"))|((?<!")(?P<value>[^,;"]*)(?!")))(?=[,;]|$)/';

	/**
	 * @param string $csv
	 * @return array
	 */
	public function convertToArray($csv) {
		$lines = explode(chr(10), $csv);
		$firstLine = trim(array_shift($lines));
		$columnNames = $this->convertLineToArray($firstLine);
		$columnCount = count($columnNames);
		$result = array();
		foreach($lines as $line) {
			$lineArray = $this->convertLineToArray(trim($line));
			if (count($lineArray) !== $columnCount) {
				continue;
			}
			$result[] = array_combine($columnNames, $lineArray);
		}
		return $result;
	}

	/**
	 * @param array $objects
	 * @param array $propertyNames
	 * @return string
	 */
	public function convertFromArray(array $objects, array $propertyNames) {
		$result = '';
			// column names
		$columnNames = array();
		foreach($propertyNames as $propertyName) {
			$columnNames[] = $this->escapeValue($propertyName);
		}
		$result .= implode(';', $columnNames) . chr(10);

			// rows
		foreach($objects as $object) {
			$cells = array();
			foreach($propertyNames as $propertyName) {
				$cells[] = $this->escapeValue(\TYPO3\FLOW3\Reflection\ObjectAccess::getPropertyPath($object, $propertyName));
			}
			$result .= implode(';', $cells) . chr(10);
		}
		return $result;
	}

	/**
	 * @param string $value
	 * @return string
	 */
	protected function escapeValue($value) {
		if (strpos($value, ';') !== FALSE) {
			$value = '"' . str_replace('"', '\"', $value) . '"';
		}
		$value = iconv('UTF-8', 'Windows-1252', $value);
		return $value;
	}

	/**
	 * @param string $line
	 * @return array
	 */
	protected function convertLineToArray($line) {
		$matches = array();
		if (preg_match_all(CsvService::PATTERN_LINE, $line, $matches) < 1) {
			return FALSE;
		}
		$result = array();
		foreach($matches['value'] as $matchIndex => $match) {
			if (strlen($match) < 1) {
				$match = $matches['quotedValue'][$matchIndex];
			}
			$result[$matchIndex] = iconv('Windows-1252', 'UTF-8//TRANSLIT', $match);;
		}
		return $result;
	}
}
?>