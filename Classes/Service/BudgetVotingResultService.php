<?php
namespace TYPO3\BccVoting\Service;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * CSV Service
 * @FLOW3\Scope("singleton")
 */
class BudgetVotingResultService {

	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\BccVoting\Domain\Repository\BudgetVoteRepository
	 */
	protected $budgetVoteRepository;

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\BudgetElection $budgetElection
	 * @return \TYPO3\BccVoting\Service\BudgetVoting\VotingResult
	 */
	public function generateVotingResult(\TYPO3\BccVoting\Domain\Model\BudgetElection $budgetElection){

		$votingResult = new \TYPO3\BccVoting\Service\BudgetVoting\VotingResult();

		$budgetVotes = $this->budgetVoteRepository->findByElection($budgetElection);

		foreach ($budgetVotes as $budgetVote) {
			$budgetRatings = $budgetVote->getBudgetRatings();
			foreach ($budgetRatings as $budgetRating) {
				$votingResult->processBudgetRating($budgetRating);
			}
			$votingResult->increaseTotalVotes();
		}

		// TODO: dirty dirty dirty... ?
		$sums = $votingResult->getSums();
		arsort($sums);
		$votingResult->setSums($sums);
		$budgetResults = $votingResult->getBudgetResults();
		foreach ($sums as $key => $dummy) {
			$sortedBudgetResults[$key] = $budgetResults[$key];
		}
		$votingResult->setBudgetResults($sortedBudgetResults);

		return $votingResult;
	}



}
