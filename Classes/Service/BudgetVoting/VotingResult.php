<?php
namespace TYPO3\BccVoting\Service\BudgetVoting;

/*                                                                        *
 * This script belongs to the FLOW3 package "BccVoting".                  *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * VotingResult Service
 */
class VotingResult {

	/**
	 * @var array
	 */
	protected $budgetResults;

	/**
	 * Used for array_multisort only
	 *
	 * @var array
	 */

	protected $sums;

	/**
	 * @var int
	 */
	protected $totalVotes;


	public function processBudgetRating(\TYPO3\BccVoting\Domain\Model\BudgetRating $budgetRating) {

		$budgetIdentity = $budgetRating->getBudget()->getIdentity();
		if (isset($this->budgetResults[$budgetIdentity])) {
			$budgetResult = $this->budgetResults[$budgetIdentity];
		} else {
			$budgetResult = new \TYPO3\BccVoting\Service\BudgetVoting\BudgetResult();
		}

		$budgetResult->setBudget($budgetRating->getBudget());

		switch ($budgetRating->getRating()) {
			case -1:
				$budgetResult->increaseAmountNegativeVotes();
				break;
			case 0:
				$budgetResult->increaseAmountNeutralVotes();
				break;
			case 1:
				$budgetResult->increaseAmountPositiveVotes();
				break;
			default:
		}

		$this->budgetResults[$budgetIdentity] = $budgetResult;
		$this->sums[$budgetIdentity] = $budgetResult->getSum();
	}

	/**
	 * @param array $budgetResults
	 */
	public function setBudgetResults($budgetResults) {
		$this->budgetResults = $budgetResults;
	}

	/**
	 * @return array
	 */
	public function getBudgetResults() {
		return $this->budgetResults;
	}

	/**
	 * @param int $totalVotes
	 */
	public function setTotalVotes($totalVotes) {
		$this->totalVotes = $totalVotes;
	}

	/**
	 * @return int
	 */
	public function getTotalVotes() {
		return $this->totalVotes;
	}

	/**
	 * @return void
	 */
	public function increaseTotalVotes() {
		$this->totalVotes++;
	}

	public function setSums($sums) {
		$this->sums = $sums;
	}

	public function getSums() {
		return $this->sums;
	}

}
