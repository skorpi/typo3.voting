<?php
namespace TYPO3\BccVoting\Service\BudgetVoting;

/*                                                                        *
 * This script belongs to the FLOW3 package "TYPO3.BccVoting".            *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;

/**
 * BudgetVotingResult for one Budget
 *
 * @FLOW3\Scope("singleton")
 */
class BudgetResult {

	/**
	 * @var \TYPO3\BccVoting\Domain\Model\Budget
	 */
	protected $budget;

	/**
	 * @var int
	 */
	protected $sum;

	/**
	 * @var int
	 */
	protected $amountNegativeVotes = 0;

	/**
	 * @var int
	 */
	protected $amountNeutralVotes = 0;

	/**
	 * @var int
	 */
	protected $amountPositiveVotes = 0;

	/**
	 * @param int $amountNegativeVotes
	 */
	public function setAmountNegativeVotes($amountNegativeVotes) {
		$this->amountNegativeVotes = $amountNegativeVotes;
	}

	/**
	 *
	 */
	public function increaseAmountNegativeVotes() {
		$this->amountNegativeVotes++;
	}

	/**
	 * @return int
	 */
	public function getAmountNegativeVotes() {
		return $this->amountNegativeVotes;
	}

	/**
	 * @param int $amountNeutralVotes
	 */
	public function setAmountNeutralVotes($amountNeutralVotes) {
		$this->amountNeutralVotes = $amountNeutralVotes;
	}

	/**
	 *
	 */
	public function increaseAmountNeutralVotes() {
		$this->amountNeutralVotes++;
	}

	/**
	 * @return int
	 */
	public function getAmountNeutralVotes() {
		return $this->amountNeutralVotes;
	}

	/**
	 * @param int $amountPositiveVotes
	 */
	public function setAmountPositiveVotes($amountPositiveVotes) {
		$this->amountPositiveVotes = $amountPositiveVotes;
	}
	/**
	 *
	 */
	public function increaseAmountPositiveVotes() {
		$this->amountPositiveVotes++;
	}

	/**
	 * @return int
	 */
	public function getAmountPositiveVotes() {
		return $this->amountPositiveVotes;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Budget $budget
	 */
	public function setBudget($budget) {
		$this->budget = $budget;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Budget
	 */
	public function getBudget() {
		return $this->budget;
	}

	/**
	 * @param \TYPO3\BccVoting\Domain\Model\Election $election
	 */
	public function setElection($election) {
		$this->election = $election;
	}

	/**
	 * @return \TYPO3\BccVoting\Domain\Model\Election
	 */
	public function getElection() {
		return $this->election;
	}


	/**
	 * @return int
	 */
	public function getSum() {
		$sum = ($this->getAmountNegativeVotes() * -1) + ($this->getAmountPositiveVotes() * 1);
		return $sum;
	}



}
