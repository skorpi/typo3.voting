<?php
namespace TYPO3\FLOW3\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
	Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20120927155627 extends AbstractMigration {

	/**
	 * @param Schema $schema
	 * @return void
	 */
	public function up(Schema $schema) {
			// this up() migration is autogenerated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
		
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_circular (flow3_persistence_identifier VARCHAR(40) NOT NULL, electorate VARCHAR(40) DEFAULT NULL, subject VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, servedemails LONGTEXT NOT NULL COMMENT '(DC2Type:array)', INDEX IDX_A48ED246393F623B (electorate), UNIQUE INDEX flow3_identity_typo3_bccvoting_domain_model_circular (subject), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_election (flow3_persistence_identifier VARCHAR(40) NOT NULL, electorate VARCHAR(40) DEFAULT NULL, title VARCHAR(255) NOT NULL, startdate DATETIME NOT NULL, enddate DATETIME NOT NULL, description LONGTEXT NOT NULL, maxnumberofvotes INT NOT NULL, INDEX IDX_1AFF120A393F623B (electorate), UNIQUE INDEX flow3_identity_typo3_bccvoting_domain_model_election (title, startdate), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_election_nominees_join (bccvoting_election VARCHAR(40) NOT NULL, bccvoting_nominee VARCHAR(40) NOT NULL, INDEX IDX_2F9D448BA15BE792 (bccvoting_election), INDEX IDX_2F9D448B39191AC (bccvoting_nominee), PRIMARY KEY(bccvoting_election, bccvoting_nominee)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_electorimport (flow3_persistence_identifier VARCHAR(40) NOT NULL, electorate VARCHAR(40) DEFAULT NULL, csvresource VARCHAR(40) DEFAULT NULL, INDEX IDX_A6234B83393F623B (electorate), INDEX IDX_A6234B83E5C5EE6E (csvresource), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_electorate (flow3_persistence_identifier VARCHAR(40) NOT NULL, title VARCHAR(255) NOT NULL, UNIQUE INDEX flow3_identity_typo3_bccvoting_domain_model_electorate (title), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_electorate_electors_join (bccvoting_electorate VARCHAR(40) NOT NULL, bccvoting_elector VARCHAR(40) NOT NULL, INDEX IDX_C6F2B1D6C24381D (bccvoting_electorate), INDEX IDX_C6F2B1DB04257B6 (bccvoting_elector), PRIMARY KEY(bccvoting_electorate, bccvoting_elector)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_image (flow3_persistence_identifier VARCHAR(40) NOT NULL, originalresource VARCHAR(40) DEFAULT NULL, title VARCHAR(255) NOT NULL, INDEX IDX_313C90B54E59BB9C (originalresource), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_token (flow3_persistence_identifier VARCHAR(40) NOT NULL, election VARCHAR(40) DEFAULT NULL, code VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_AB3635D1DCA03800 (election), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_vote (flow3_persistence_identifier VARCHAR(40) NOT NULL, election VARCHAR(40) DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_8F313E72DCA03800 (election), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_vote_nominees_join (bccvoting_vote VARCHAR(40) NOT NULL, bccvoting_nominee VARCHAR(40) NOT NULL, INDEX IDX_CEBA4DEEB02DE17 (bccvoting_vote), INDEX IDX_CEBA4DEE39191AC (bccvoting_nominee), PRIMARY KEY(bccvoting_vote, bccvoting_nominee)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_person (flow3_persistence_identifier VARCHAR(40) NOT NULL, gender VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, middlename VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_elector (flow3_persistence_identifier VARCHAR(40) NOT NULL, PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("CREATE TABLE typo3_bccvoting_domain_model_nominee (flow3_persistence_identifier VARCHAR(40) NOT NULL, image VARCHAR(40) DEFAULT NULL, url VARCHAR(255) NOT NULL, comments LONGTEXT NOT NULL, public TINYINT(1) NOT NULL, INDEX IDX_AC230A19C53D045F (image), PRIMARY KEY(flow3_persistence_identifier)) ENGINE = InnoDB");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_circular ADD CONSTRAINT FK_A48ED246393F623B FOREIGN KEY (electorate) REFERENCES typo3_bccvoting_domain_model_electorate (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_election ADD CONSTRAINT FK_1AFF120A393F623B FOREIGN KEY (electorate) REFERENCES typo3_bccvoting_domain_model_electorate (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_election_nominees_join ADD CONSTRAINT FK_2F9D448BA15BE792 FOREIGN KEY (bccvoting_election) REFERENCES typo3_bccvoting_domain_model_election (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_election_nominees_join ADD CONSTRAINT FK_2F9D448B39191AC FOREIGN KEY (bccvoting_nominee) REFERENCES typo3_bccvoting_domain_model_nominee (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_electorimport ADD CONSTRAINT FK_A6234B83393F623B FOREIGN KEY (electorate) REFERENCES typo3_bccvoting_domain_model_electorate (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_electorimport ADD CONSTRAINT FK_A6234B83E5C5EE6E FOREIGN KEY (csvresource) REFERENCES typo3_flow3_resource_resource (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_electorate_electors_join ADD CONSTRAINT FK_C6F2B1D6C24381D FOREIGN KEY (bccvoting_electorate) REFERENCES typo3_bccvoting_domain_model_electorate (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_electorate_electors_join ADD CONSTRAINT FK_C6F2B1DB04257B6 FOREIGN KEY (bccvoting_elector) REFERENCES typo3_bccvoting_domain_model_elector (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_image ADD CONSTRAINT FK_313C90B54E59BB9C FOREIGN KEY (originalresource) REFERENCES typo3_flow3_resource_resource (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_token ADD CONSTRAINT FK_AB3635D1DCA03800 FOREIGN KEY (election) REFERENCES typo3_bccvoting_domain_model_election (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_vote ADD CONSTRAINT FK_8F313E72DCA03800 FOREIGN KEY (election) REFERENCES typo3_bccvoting_domain_model_election (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_vote_nominees_join ADD CONSTRAINT FK_CEBA4DEEB02DE17 FOREIGN KEY (bccvoting_vote) REFERENCES typo3_bccvoting_domain_model_vote (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_vote_nominees_join ADD CONSTRAINT FK_CEBA4DEE39191AC FOREIGN KEY (bccvoting_nominee) REFERENCES typo3_bccvoting_domain_model_nominee (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_elector ADD CONSTRAINT FK_1FF0CC0321E3D446 FOREIGN KEY (flow3_persistence_identifier) REFERENCES typo3_bccvoting_domain_model_person (flow3_persistence_identifier) ON DELETE CASCADE");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_nominee ADD CONSTRAINT FK_AC230A19C53D045F FOREIGN KEY (image) REFERENCES typo3_bccvoting_domain_model_image (flow3_persistence_identifier)");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_nominee ADD CONSTRAINT FK_AC230A1921E3D446 FOREIGN KEY (flow3_persistence_identifier) REFERENCES typo3_bccvoting_domain_model_person (flow3_persistence_identifier) ON DELETE CASCADE");
	}

	/**
	 * @param Schema $schema
	 * @return void
	 */
	public function down(Schema $schema) {
			// this down() migration is autogenerated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
		
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_election_nominees_join DROP FOREIGN KEY FK_2F9D448BA15BE792");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_token DROP FOREIGN KEY FK_AB3635D1DCA03800");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_vote DROP FOREIGN KEY FK_8F313E72DCA03800");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_circular DROP FOREIGN KEY FK_A48ED246393F623B");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_election DROP FOREIGN KEY FK_1AFF120A393F623B");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_electorimport DROP FOREIGN KEY FK_A6234B83393F623B");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_electorate_electors_join DROP FOREIGN KEY FK_C6F2B1D6C24381D");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_nominee DROP FOREIGN KEY FK_AC230A19C53D045F");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_vote_nominees_join DROP FOREIGN KEY FK_CEBA4DEEB02DE17");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_elector DROP FOREIGN KEY FK_1FF0CC0321E3D446");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_nominee DROP FOREIGN KEY FK_AC230A1921E3D446");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_electorate_electors_join DROP FOREIGN KEY FK_C6F2B1DB04257B6");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_election_nominees_join DROP FOREIGN KEY FK_2F9D448B39191AC");
		$this->addSql("ALTER TABLE typo3_bccvoting_domain_model_vote_nominees_join DROP FOREIGN KEY FK_CEBA4DEE39191AC");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_circular");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_election");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_election_nominees_join");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_electorimport");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_electorate");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_electorate_electors_join");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_image");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_token");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_vote");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_vote_nominees_join");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_person");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_elector");
		$this->addSql("DROP TABLE typo3_bccvoting_domain_model_nominee");
	}
}

?>