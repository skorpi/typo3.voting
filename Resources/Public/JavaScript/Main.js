$(function() {
	$('.flashmessages').delay(2500).fadeOut('slow');
	$('.multiselect').multiselect({sortable: false});
	$('.toggleComments').click(function(event) {
		event.preventDefault();
		$('.comment').slideToggle('300', 'linear');
	});
});